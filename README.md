## Tequila

Tequila is a computer program for the calculation of the linear response of an atomic lattice with interacting permanent charges via induced dipoles through the Micro-Electrostatics framework
(ME). The program (as well as its name) originates as a branch of MESCal1 [1]. It features the possibility of assigning independent anisotropic polarizabilities and charges to each site, and
solves the system by direct matrix inversion instead of the iterative algorithm used in MESCal. While computationally more expensive for large systems, this approach has the advantage of
being more robust for unstable systems, since iterative algorithms are known to be sensitive to initial conditions and sometimes feature convergence problems. A computational advantage
of Tequila is that electrostatic sums are only performed once. Furthermore, Tequila can give access to the spectrum of the Hessian as to allow an in-depth analysis of such systems [2].
Tequila is entirely written in C++11 and relies extensively on the BLAS and LAPACK interfaces for linear algebra subroutines. It also implements shared memory parallelization through OpenMP.

[1] Gabriele D’Avino, Luca Muccioli, Claudio Zannoni, David Beljonne, and Zoltán G. Soos. *Electronic polarization in organic crystals: A comparative study of induced dipoles and intramolecular
charge redistribution schemes*. Journal of Chemical Theory and Computation (2014).

[2] Massimiliano Comin, Simone Fratini, Xavier Blase, Gabriele D'Avino. *Doping-Induced Dielectric Catastrophe Prompts Free-Carrier Release in Organic Semiconductors*. Advanced Materials (2021).


## Requirements

## Installation

## Basic Usage

## Contributing - Roadmap

## License
See the [LICENSE](LICENSE) file for licensing information as it pertains to files in this repository.

## Author
Massimiliano Comin
