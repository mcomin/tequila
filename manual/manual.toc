\babel@toc {english}{}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Theoretical Background}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Derivation from Linear Response}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Derivation from the Total Energy}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Periodic Boundary Conditions}{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Solution of the Linear System and Dielectric Response}{5}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}Resolution methods}{5}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}Dielectric Response}{5}{subsubsection.2.4.2}
\contentsline {subsubsection}{\numberline {2.4.3}Spectral Decomposition of $\relax $\@@underline {\hbox {\bm {\zeta }}}\mathsurround \z@ $\relax $}{6}{subsubsection.2.4.3}
\contentsline {section}{\numberline {3}Compiling and Testing Tequila}{8}{section.3}
\contentsline {section}{\numberline {4}Usage}{9}{section.4}
\contentsline {subsection}{\numberline {4.1}The input file}{10}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}The data file}{12}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Tequila's output files}{13}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Runtime output}{13}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}Main output}{13}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3}Potentials and Fields}{13}{subsubsection.4.3.3}
\contentsline {subsubsection}{\numberline {4.3.4}Hessian Matrix and Source Vector}{14}{subsubsection.4.3.4}
\contentsline {subsubsection}{\numberline {4.3.5}Eigenvectors and Eigenvalues}{14}{subsubsection.4.3.5}
\contentsline {subsubsection}{\numberline {4.3.6}Response}{14}{subsubsection.4.3.6}
