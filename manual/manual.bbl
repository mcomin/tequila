\begin{thebibliography}{3}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[Dressel and Grüner()]{dressel_electrodynamics_2002}
Martin Dressel and George Grüner.
\newblock \emph{Electrodynamics of solids: optical properties of electrons in
  matter}.
\newblock Cambridge University Press.
\newblock ISBN 978-0-521-59253-6 978-0-521-59726-5.

\bibitem[D’Avino et~al.({\natexlab{a}})D’Avino, Muccioli, Zannoni,
  Beljonne, and Soos]{mescal}
Gabriele D’Avino, Luca Muccioli, Claudio Zannoni, David Beljonne, and
  Zoltán~G. Soos.
\newblock Electronic polarization in organic crystals: A comparative study of
  induced dipoles and intramolecular charge redistribution schemes.
\newblock 10\penalty0 (11):\penalty0 4959--4971, {\natexlab{a}}.
\newblock ISSN 1549-9618, 1549-9626.
\newblock \doi{10.1021/ct500618w}.
\newblock URL \url{https://pubs.acs.org/doi/10.1021/ct500618w}.

\bibitem[D’Avino et~al.({\natexlab{b}})D’Avino, Vanzo, and
  Soos]{dielectric}
Gabriele D’Avino, Davide Vanzo, and Zoltán~G. Soos.
\newblock Dielectric properties of crystalline organic molecular films in the
  limit of zero overlap.
\newblock 144\penalty0 (3):\penalty0 034702, {\natexlab{b}}.
\newblock ISSN 0021-9606, 1089-7690.
\newblock \doi{10.1063/1.4939840}.
\newblock URL \url{http://aip.scitation.org/doi/10.1063/1.4939840}.

\end{thebibliography}
