/*
Tequila: Micro-Electrostatics Software
Copyright (C) 2021  Massimiliano Comin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact: https://gitlab.com/mcomin/tequila/
*/
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "module_io.h"

using namespace std;

void print_header()
{
    printf("================================================================================\n");
    printf("\t\t\t _______               _ _\n"
            "\t\t\t|__   __|             (_) |\n"
            "\t\t\t   | | ___  __ _ _   _ _| | __ _\n"
            "\t\t\t   | |/ _ \\/ _` | | | | | |/ _` |\n"
            "\t\t\t   | |  __/ (_| | |_| | | | (_| |\n"
            "\t\t\t   |_|\\___|\\__, |\\__,_|_|_|\\__,_|\n"
            "\t\t\t              | |\n"
            "\t\t\t              |_|\n");

    printf("================================================================================\n");
    center_text("Tequila  Copyright (C) 2021  Massimiliano Comin");
    center_text("This program comes with ABSOLUTELY NO WARRANTY.");
    center_text("For details type `tequila --license`.\n");
}

void print_license()
{
    std::ifstream f("LICENSE");

    if (f.is_open())
    {
        std::cout << f.rdbuf();
    }
    else
    {
        printf("FATAL ERROR: Cannot find License file.\n");
        printf("You may have not received the original version of this software.\n");
        printf("Please visit https://gitlab.com/mcomin/tequila/ to find the original version.\n");
    }

}


// Center text on a line of 80 characters
void center_text(const char* text)
{
    int padlen = (80 - strlen(text)) / 2;
    printf("%*s%s%*s\n", padlen, "", text, padlen, "");
}

void reading_text(const char* text)
{
    int padlen = (80 - strlen(text) - 8) / 2;
    printf("%*sReading %s%*s\n\n", padlen, "", text, padlen, "");
}

string getFileName(const string path)
{
    string name;
    size_t sep;

    sep = path.find_last_of('/');
    if (sep == string::npos)
    {
        sep = path.find_last_of('.');
        if (sep == string::npos)
        {
            return path;
        }
        else
        {
            return path.substr(0,sep);
        }
    }
    else
    {
        name = path.substr(sep,path.size());
        sep = name.find_last_of('.');
        if (sep == string::npos)
        {
            return name.substr(1,name.size());
        }
        else
        {
            return name.substr(1,sep-1);
        }
    }
}



int read_input(const char* filename, string& data_file, double& PBC_radius, double* PBC_box,
                                     double& epsilon, double* F_ext, bool& writeT, bool& loadT, bool& loadS, bool& Fulldiag,
                                     bool& response, double* depol, double& hack)
{
    // Read input file //
    reading_text(filename);
    ifstream file(filename);

    if (!file.is_open())
    {
      printf("FATAL ERROR: Failed to open %s\n",filename);
      return 1;
    }

    string line;
    size_t pos;

    printf("--------------------------------------------------------------------------------\n");

    while (getline(file, line))
    {

        cout << line << endl;

        if (line.empty())
        {
            continue;
        }

        if (line.find("INPUT") != string::npos)
        {
            if ((pos = line.find(" ")) != std::string::npos)
            {
                line.erase(0, pos + 1);
                data_file = line;
            }
        }

        if (line.find("PBC") != string::npos)
        {
            if ((pos = line.find(" ")) != std::string::npos)
            {
                line.erase(0, pos + 1);
                PBC_radius = atof(line.c_str());
            }
        }

        if (line.find("BOX") != string::npos)
        {
            for (int i=0; i<3; i++)
            {
                getline(file, line);
                istringstream iss(line);

                cout << line << endl;

                for (int j=0; j<3; j++)
                {
                    iss >> PBC_box[3*i+j];
                }
            }
        }

        if (line.find("EPSILON") != string::npos)
        {
            if ((pos = line.find(" ")) != std::string::npos)
            {
                line.erase(0, pos + 1);
                epsilon = atof(line.c_str());
            }
        }

        if (line.find("FEXT") != string::npos)
        {
            for (int i=0; i<3; i++)
            {
                if ((pos = line.find(" ")) != std::string::npos)
                {
                    pos = line.find(" ");
                    line.erase(0, pos + 1);
                    F_ext[i] = atof(line.c_str());
                }
            }
        }

        if (line.find("WRITET") != string::npos)
        {
            writeT = true;
        }

        if (line.find("LOADT") != string::npos)
        {
            loadT = true;
        }

        if (line.find("LOADS") != string::npos)
        {
            loadS = true;
        }

        if (line.find("FULLDIAG") != string::npos)
        {
            Fulldiag = true;
        }

        if (line.find("RESPONSE") != string::npos)
        {
            response = true;
        }

        if (line.find("DEPOL") != string::npos)
        {
            for (int i=0; i<3; i++)
            {
                getline(file, line);
                istringstream iss(line);

                cout << line << endl;

                for (int j=0; j<3; j++)
                {
                    iss >> depol[3*i+j];
                }
            }
        }


        if (line.find("HACK") != string::npos)
        {
            if ((pos = line.find(" ")) != std::string::npos)
            {
                line.erase(0, pos + 1);
                hack = atof(line.c_str());
            }
        }

    }

    file.close();
    printf("--------------------------------------------------------------------------------\n\n");
    printf("=> Input file read successfully!\n");

    return 0;

}






int read_datainput(const string& filename, double*& X, double*& Q, bool*& B, double*& P, int& n, int& m)
{

    double total_charge = 0.;

    reading_text(filename.c_str());

    // Read input file //
    ifstream file(filename);

    if (!file.is_open())
    {
      printf("FATAL ERROR: Failed to open %s\n",filename.c_str());
      return 1;
    }

    // Read n rows, m columns //
    string line;
    getline(file, line);

    if (line.find('#') != string::npos )
    {
        line.assign(line.substr(line.find('#')+1,string::npos));
    }

    istringstream iss(line);
    iss >> n >> m;

    if (!((m == 5) || (m == 10)))
    {
        printf("FATAL ERROR: Incorrect values of (nrows, ncols) in header.\n");
        printf(" Input file must have 5 or 10 columns.\n");
        return 1;
    }
    else
    {
        printf("=> Number of sites: %d\n",n);
    }

    X = new double[n*3];
    Q = new double[n];
    P = new double[n*(m-4)];
    B = new bool[n];

    // Read data
    for (int i=0; i<n; i++)
    {
        for (int j=0; j<3; j++)
        {
            file >> X[i*3+j];
        }

        file >> Q[i];
        total_charge += Q[i];
        if (Q[i] != 0.)
        {
            B[i] = true;
        }
        else
        {
            B[i] = false;
        }

        if (m == 10)
            for (int j=0; j<m-4; j++)
            {
                file >> P[i*(m-4)+j];
            }
        else // i.e. m == 5
        {
            file >> P[i];
        }
    }

    if (file.fail() || file.eof())
    {
        printf("FATAL ERROR: Failed to read %s\n",filename.c_str());
        delete [] X;
        delete [] Q;
        delete [] P;
        return 1;
    }

    file.close();

    printf("=> Total charge: %f\n",total_charge);
    printf("=> Data file read successfully!\n\n");

    return 0;

}


int read_data(const char* filename, double* X)
{
    ifstream file(filename);

    if (!file.is_open())
    {
        return 1;
    }

    file.seekg(0, ios_base::end);
    size_t size = file.tellg();
    file.seekg(0, ios_base::beg);

    file.read((char*) &X[0], size);
    file.close();

    return 0;

}


void save_data(const char* filename, const double* x, const int& N)
{
    auto file = fstream(filename, ios::out | ios::binary);
    file.write((char*)&x[0], N*sizeof(double));
    file.close();
}




int save_vector(const char* filename, const double* x, const int& N)
{

    FILE * file;
    file = fopen(filename,"w");

    for (int i=0; i<N; i++)
    {
        fprintf(file, " %-24.12f \n", x[i]);
    }

    fclose(file);

    return 0;
}

// Save Matrix assuming Row-Major order
int save_matrix(const char* filename, const double* x, const int& N)
{
    FILE * file;
    file = fopen(filename,"w");

    for (int i=0; i<N; i++)
    {
        for  (int j=0; j<N; j++)
        {
            fprintf(file, " %-24.12f ", x[i+N*j]);
        }
        fprintf(file, "\n");
    }

    fclose(file);


    return 0;
}


int save_potential_field(const char* filename, const double* V, const double* F, const int& n)
{
    FILE * file;
    file = fopen(filename,"w");

    fprintf(file,"# V Fx Fy Fz\n");

    for (int i=0; i<n; i++)
    {
        fprintf(file, " %-24.12f ", V[i]);

        for  (int j=0; j<3; j++)
        {
            fprintf(file, " %-24.12f ", F[j+3*i]);
        }

        fprintf(file, "\n");
    }

    fclose(file);

    return 0;
}





int write_output(const string name, const double* X, const double* Q, const double* M, const int& n)
{

    FILE * file;
    file = fopen((name + ".dat").c_str(),"w");

    fprintf(file, "# X Y Z Q MuX MuY MuZ\n");

    for (int i=0; i<n; i++)
    {
        // Write X,Y,Z
        for  (int j=0; j<3; j++)
        {
            fprintf(file, " %16.8f ", X[j+3*i]);
        }

        // Write charge
        fprintf(file, " %16.8f ", Q[i]);

        // Write dipole
        for  (int j=0; j<3; j++)
        {
            fprintf(file, " %24.12f ", M[j+3*i]);
        }

        fprintf(file, "\n");
    }

    fclose(file);

    return 0;

}
