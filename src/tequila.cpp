/*
Tequila: Micro-Electrostatics Software
Copyright (C) 2021  Massimiliano Comin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact: https://gitlab.com/mcomin/tequila/
*/
#include <stdlib.h>
#include <fstream>
#include <algorithm>
#include <array>
#include <chrono>
#include <omp.h>
#include <cblas.h>
#include "module_io.h"
#include "module_math.h"

using namespace std;

// TODO: Resolve function | Print matrix only function


// Overload timer in case of non OpenMP compilation
#ifndef _OPENMP
std::chrono::steady_clock::time_point t0 = std::chrono::steady_clock::now();

double omp_get_wtime()
{
    return (double)std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - t0).count()/1e6;
}
#endif




int main(int argc, char* argv[])
{
    // Print Header & version
    print_header();
    center_text("Version 04/01/2022");
    #ifdef _OPENMP
        center_text("Compiled with OpenMP");
    #endif
    printf("================================================================================\n");

    int info;

    // Parse input arguments
    if ((argc == 1) || string(argv[1]) == "--help")
    {
        center_text("Usage:");
        printf("\ntequila file.inp\tRead input file and execute program\n");
        #ifdef _OPENMP
            printf("tequila -np N file.inp\tRead input file and execute program on N threads\n");
        #endif
        printf("tequila --test \t\tRun unit tests (only from install dir)\n");
        printf("tequila --help \t\tShow this message and exit\n");
        exit(0);
    }
    else if (string(argv[1]) == "--test")
    {
        center_text("You launched Tequila Unit tests.");
        printf("\n");
        info = system("src/module_test.py");
        if (info != 0)
        {
            exit(info);
        }
        return 0;
    }
    else if ((string(argv[1]) == "-np") || (string(argv[1]) == "-n"))
    {
        if (argc < 4)
        {
            printf("FATAL ERROR: Tequila called with too few arguments.\n");
            exit(1);
        }
        #ifdef _OPENMP
            omp_set_num_threads(atoi(argv[2]));
            argv[1] = argv[3];
        #endif

        #ifndef _OPENMP
            printf("=> WARNING: Invalid argument -n or -np:\n   Tequila must be compiled with OpenMP.");
            printf("Value not contemplated.\n");
            argv[1] = argv[3];
        #endif
    }
    else if (string(argv[1]) == "--license")
    {
        print_license();
        return 0;
    }
    else
    {
        // Set Number of OpenMP threads
        #ifdef _OPENMP
        if (getenv("OMP_NUM_THREADS"))
        {
            omp_set_num_threads(atoi(getenv("OMP_NUM_THREADS")));
        }
        else
        {
            center_text("OMP_NUM_THREADS is not set");
        }
        #endif
    }

    #ifdef _OPENMP
        string str = "Running on "+to_string(omp_get_max_threads()) + " threads";
        center_text(str.c_str());
        printf("================================================================================\n");
    #endif



    // Initialize Timers
    double total_time1 = omp_get_wtime();
    double t1 = 0.;
    double t2 = 0.;


    string name = getFileName(string(argv[1]));     // Initialize file name
    string data_file;                               // Initialize data file
    bool response = false;                          // Initialize Response switch
    bool writeT = false;                             // Initialize save T switch
    bool loadT = false;                             // Initialize load T switch
    bool loadS = false;                             // Initialize load Q switch
    bool PBC = false;                               // Initialize PBC switch
    bool fulldiag = false;                          // Initialize Full diagonalization switch
    int count = 0;                                  // Initialize Instabilities counter
    char suffix[3] = {'X','Y','Z'};


    double PBC_radius = 0.;                             // Initialize PBC sphere radius
    double PBC_box[9] = {0.,0.,0.,0.,0.,0.,0.,0.,0.};   // Initialize PBC cell vectors
    double epsilon = 1.;                                // Initialize dielectric constant
    double F_ext[3] = {0., 0., 0.};                     // Initialize External Field
    double depol[9] = {0.,0.,0.,0.,0.,0.,0.,0.,0.};     // Initialize Depolarization tensor
    double hack = 0.;                                   // Initialize Hacky parameter (DANGER :D)
    char _str[30];                                      // Initialize multi-purpose string

    int n, m;       // Initialize rows & cols
    int N;          // Initialize number of degrees of freedom
    double* X;      // Initialize XYZ Positions
    double* Q;      // Initialize Charges
    double* P;      // Initialize Polarizabilities
    double* T;      // Initialize Interaction matrix
    double* S;      // Initialize Source vector
    double* M;      // Initialize Dipoles
    double* W;      // Initialize Eigenvalues (for full diagonalization)
    double* V0;     // Initialize Initial Potentials
    double* F0;     // Initialize Initial Fields
    double* V;      // Initialize Final Potentials
    double* F;      // Initialize Final Fields
    double* Z;      // Initialize Zeta
    double* R;      // Initialize partial sums of inverse hessian
    bool* B;        // Initialize Boolean Charges


    // Initialize total energies
    double E_electrostatic;
    double E_induction;
    double E_total;






    // Read input file
    info = read_input(argv[1], data_file, PBC_radius, PBC_box, epsilon, F_ext, writeT, loadT, loadS, fulldiag, response, depol, hack);
    if (info != 0)
    {
        exit(info);
    }
    else
    {
        if (loadS)
        {
            if (response)
            {
                printf("=> WARNING: LOADS instruction is incompatible with RESPONSE. LOADS will be ignored.\n");
            }
            else
            {
                printf("=> Will read source vector from S.dat !\n");
            }
        }

        if (writeT)
        {
            printf("=> WriteT: WARNING: The calculation will STOP after writing T.dat !\n");
        }

        if (loadT)
        {
            printf("=> LoadT: Will read inteaction matrix from T.dat !\n");
        }

        if (fulldiag)
        {
            printf("=> Fulldiag: Full diagonalization turned ON !\n");
        }

        if (hack != 0.)
        {
            printf("=> Hack: Interaction cutoff set to: %f !\n",hack);
        }

        if (response)
        {
            printf("=> Response: option enabled !\n");
            printf("   (Input charges will be ignored)\n\n");

            if (PBC_radius == 0.)
            {
                printf("FATAL ERROR: No PBC instruction found. Cannot perform RESPONSE calculation.\n");
                exit(1);
            }

            PBC = true;

        }

        if (PBC_radius != 0.)
        {
            printf("=> PBC: Radius set to:\t%f Å\n",PBC_radius);
            PBC_radius += 1e-10;

            if (all_of(PBC_box, PBC_box+9, [](double x) {return x==0.;}))
            {
                printf("=> FATAL ERROR: PBC have been enabled but the PBC vectors have not been set, or are all 0-vectors.\n");
                exit(1);
            }
            else
            {
                printf("=> BOX: PBC vectors have been set to (row-major) (Å):\n");
                for (int i=0; i<3; i++)
                {
                    printf("  %-8.3f %-8.3f %-8.3f\n",PBC_box[3*i],PBC_box[3*i+1],PBC_box[3*i+2]);
                }
            }

            PBC = true;

            printf("\n");

        }

        if (epsilon != 1.)
        {
            printf("=> Epsilon: Relative permittivity set to:\t%f\n",epsilon);
        }

        if (!all_of(F_ext, F_ext+3, [](double x) {return x==0.;}))
        {
            if (!response)
            {
                printf("=> Fext: External Field set to (V/Å):\n");
                printf("\t%f %f %f\n",F_ext[0],F_ext[1],F_ext[2]);
            }
            else
            {
                printf("=> WARNING: FEXT instruction is incompatible with RESPONSE. FEXT will be ignored.\n");
            }

        }

        if (any_of(depol, depol+9, [](double x) {return x!=0.;}))
        {
            // Normalize trace to one
            double depol_trace = depol[0] + depol[4] + depol[8];

            for (int i=0; i<9; i++)
            {
                depol[i] /= depol_trace;
            }

            printf("=> Depol: Depolarization tensor set to (row-major):\n");
            for (int i=0; i<3; i++)
            {
                printf("  %-8.3f %-8.3f %-8.3f\n",depol[3*i],depol[3*i+1],depol[3*i+2]);
            }
            printf("  (Trace has been normalized to 1)\n");

            if (all_of(PBC_box, PBC_box+9, [](double x) {return x==0.;}))
            {
                printf("FATAL ERROR: No BOX instruction found, or all vectors are 0. Cannot include depolarization.\n");
                exit(1);
            }

            if (!response)
            {
                printf("=> WARNING: Ignoring DEPOL instruction because RESPONSE is not set.\n");
            }

            PBC = true;

            printf("\n");
        }

        printf("\n");
    }
    printf("================================================================================\n");


    // Read data file
    info = read_datainput(data_file,X,Q,B,P,n,m);
    if (info != 0)
    {
        exit(info);
    }
    else
    {
        // Matrix size
        N = 3 * n;
    }



    // Deallocate
    if (response)
    {
        delete [] B;
        delete [] Q;
    }















    // Build Interaction Matrix //
    printf("================================================================================\n");
    t1 = omp_get_wtime();

    if (!loadT)
    {
        center_text("Building Interaction Matrix");
        printf("\n");
        printf("Matrix Size: %dx%d\n",N,N);

        T = new double[N*N]{0.};

        if (PBC)
        {
            interaction_matrix(X, P, T, PBC_radius, PBC_box, epsilon, m, n, hack);
        }
        else
        {
            interaction_matrix(X, P, T, epsilon, m, n, hack);
        }


        printf("Saving Interaction Matrix to T.dat\n");
        save_data("T.dat",T,N*N);
    }

    // Read Interaction Matrix //
    else
    {
        reading_text("T.dat");
        printf("\n");
        printf("Expected Interaction Matrix Size: %dx%d\n",N,N);

        T = new double[N*N]{0.};
        info = read_data("T.dat",T);

    }

    t2 = omp_get_wtime();
    printf("\nTime (s): %-10.3f\n",t2-t1);



    // Deallocation
    if (info == 0 && !writeT)
    {
        if (response)
        {
            delete [] X;
            delete [] P;
        }
        else
        {
            delete [] P;
        }
    }
    else
    {

        if (writeT)
        {
            // Exit
            printf("================================================================================\n");
            double total_time2 = omp_get_wtime();
            printf("\nTotal time (s): %-10.3f\n\n",total_time2-total_time1);

            center_text("SUCCESS: Program returned normally !");
            printf("================================================================================\n");
        }

        if (response)
        {
            delete [] X;
            delete [] P;
            delete [] T;
        }
        else
        {
            delete [] X;
            delete [] Q;
            delete [] B;
            delete [] P;
            delete [] T;
        }
        exit(info);
    }

















    // Build Source Vector //
    if (!(response && fulldiag))
    {
    printf("================================================================================\n");
    t1 = omp_get_wtime();

    // Zeta calculation//
    if (response && !fulldiag)
    {
        E_electrostatic = 0.;

        center_text("Building Response Vectors along X Y Z");
        printf("\n");
        printf("Vector Size: %dx3\n",N);

        S = new double[N*3];
        M = new double[N*3];


        // Different Source Vectors are in Column-Major order
        for (int nrhs=0; nrhs<3; nrhs++)
        {
            #pragma omp parallel for
            for (int i=0; i<n; i++)
            {
                for (int j=0; j<3; j++)
                {
                    if (nrhs == j)
                    {
                        S[N*nrhs+3*i+j] = 1. / epsilon;
                    }
                    else
                    {
                        S[N*nrhs+3*i+j] = 0.;
                    }
                    M[N*nrhs+3*i+j] = S[N*nrhs+3*i+j];
                }
            }
        }

    }
    // Normal calculation //
    else if (!response)
    {

        S = new double[N]{0.};
        M = new double[N];
        V0 = new double[n]{0.};
        F0 = new double[N];

        if (!loadS)
        {
            center_text("Building Source Vector");
            printf("\n");
            printf("Vector Size: %d\n",N);

            if (PBC)
            {
                source_vector(X, Q, B, S, V0, F0, PBC_radius, PBC_box, epsilon, F_ext, n);
            }
            else
            {
                source_vector(X, Q, B, S, V0, F0, epsilon, F_ext, n);
            }

            printf("Saving Source Vector to S.dat\n");
            save_data("S.dat",S,N);
            printf("Saving initial Potentials and Fields to potential_field0.dat\n");
            save_potential_field("potential_field0.dat", V0, F0, n);

            // Copy to M
            copy(S,S+N,M);
        }

        // Read Source Vector //
        else
        {
            reading_text("S.dat");
            printf("\n");
            printf("Expecting Source Vector Size: %d\n",N);

            info = read_data("S.dat",S);

            // Copy to M
            copy(S,S+N,M);

            // Rebuild potential
            printf("Rebuilding initial Potentials and fields\n");
            if (PBC)
            {
                potential_field(X,Q,F_ext,V0,F0,PBC_radius,PBC_box,epsilon,n);
            }
            else
            {
                potential_field(X,Q,F_ext,V0,F0,epsilon,n);
            }

            printf("Saving initial Potentials and Fields to potential_field0.dat\n");
            save_potential_field("potential_field0.dat", V0, F0, n);

        }

    }


    t2 = omp_get_wtime();
    printf("\nTime (s): %-10.3f\n",t2-t1);

    // Deallocation
    if (info == 0)
    {
        if (!response)
        {
            delete [] B;
        }
    }
    else
    {

        if (response)
        {
            delete [] T;
            delete [] S;
            delete [] M;
        }
        else
        {
            delete [] X;
            delete [] Q;
            delete [] B;
            delete [] T;
            delete [] S;
            delete [] M;
            delete [] V0;
            delete [] F0;
        }
        exit(info);
    }
    }




















    // Solve eigensystem
    if (fulldiag)
    {
        printf("================================================================================\n");
        center_text("Diagonalizing Interaction Matrix");

        t1 = omp_get_wtime();

        W = new double[N];
        R = new double[N*3];

        info = eigen(T,W,N);

        // Check that T is positive definite
        #pragma omp parallel for simd reduction(+:count)
        for (int i=0; i<N; ++i)
        {
            if (W[i] <= 0.)
            {
                count += 1;
            }
        }

        printf("\nMinimum eigenvalue (eV/(eÅ)²): \t\t %20.8f \n", W[0]);
        printf("Maximum eigenvalue (eV/(eÅ)²): \t\t %20.8f \n", W[N-1]);
        printf("Instabilities: %d\n",count);

        if (count == 0)
        {
            printf("\n=> Interaction Matrix is positive definite!\n");
        }
        else
        {
            printf("\n=> WARNING: Interaction Matrix is not positive definite!\n");
            printf("=> The linear system will be solved on the subspace of positive eigenvalues !\n\n");
        }

        printf("\nSaving eigenvalues to eigvals.dat\n");
        save_data("eigvals.dat", W, N);
        printf("\nSaving eigenvectors to eigvecs.dat\n");
        save_data("eigvecs.dat", T, N*N);

        eigenvector_sum(T,R,n);
        printf("Saving eigenvector sums along X,Y,Z to eigvecsum.dat\n");
        save_data("eigvecsum.dat",R,N*3);


        if (info == 0)
        {
            if (response)
            {
                delete [] T;
            }
            else
            {
                delete [] R;
            }
        }
        else
        {
            if (response)
            {
                delete [] T;
                delete [] W;
                delete [] R;
            }
            else
            {
                delete [] X;
                delete [] Q;
                delete [] S;
                delete [] M;
                delete [] V0;
                delete [] F0;
                delete [] T;
                delete [] W;
                delete [] R;
            }
            exit(info);
        }

        t2 = omp_get_wtime();
        printf("\nTime (s): %-10.3f\n",t2-t1);

    }
























    if (!(fulldiag && response))
    {
        printf("================================================================================\n");
        center_text("Solving Linear System");
        t1 = omp_get_wtime();

        if (response)
        {
            // Compute partial sums of Hessian into M
            info = inverse(T,M,N,3);
        }
        else
        {
            // Solve linear system from Eigen decomposition
            if (fulldiag)
            {
                solve(T,W,S,M,N);
            }
            // Solve linear system by Cholesky factorization
            else
            {
                info = inverse(T,M,N,1);
            }
        }

        t2 = omp_get_wtime();
        printf("\nTime (s): %-10.3f\n",t2-t1);

        if (info == 0)
        {
            if (response)
            {
                delete [] T;
            }
            else
            {
                if (fulldiag)
                {
                    delete [] T;
                    delete [] W;
                }
                else
                {
                    delete [] T;
                }
            }
        }
        else
        {
            if (response)
            {
                delete [] T;
                delete [] S;
                delete [] M;
            }
            else
            {
                if (fulldiag)
                {
                    delete [] W;
                }
                delete [] T;
                delete [] W;
                delete [] X;
                delete [] Q;
                delete [] S;
                delete [] M;
                delete [] V0;
                delete [] F0;
            }
            exit(info);
        }

    }




















    t1 = omp_get_wtime();

    // Response
    if (response)
    {
        printf("================================================================================\n");
        center_text("Response");

        double Vol = volume(PBC_box);
        Z = new double[9];

        if (fulldiag)
        {
            // Compute Zeta from spectral decomposition
            spectral_zeta(R, W, Z, Vol, epsilon, N);

        }
        else
        {
            // Compute Zeta from inversion
            inverse_zeta(S, M, Z, Vol, epsilon, N);
        }


        // Print Matrix
        printf("\nZeta Matrix (unitless):\n");

        for (int i=0; i<3; ++i)
        {
            printf("  %-12.8f %-12.8f %-12.8f\n",Z[3*i+0],Z[3*i+1],Z[3*i+2]);
        }

        printf("\nSaving Zeta to zeta.dat\n");
        save_matrix("zeta.dat",Z,3);


        // Compute Chi if depol is given
        if (any_of(depol, depol+9, [](double x) {return x!=0.;}))
        {

            double* Chi = new double[9]{0.};

            depolarization(Z,depol,Chi);

            printf("Chi Matrix (unitless):\n");

            // Print Matrix
            for (int i=0; i<3; ++i)
            {
                printf("  %-12.8f %-12.8f %-12.8f\n",Chi[3*i+0],Chi[3*i+1],Chi[3*i+2]);
            }

            printf("\nSaving Chi to chi.dat\n");
            save_matrix("chi.dat",Chi,3);

            delete [] Chi;

        }

        // Deallocate
        delete [] Z;

        if (fulldiag)
        {
            delete [] W;
            delete [] R;
        }
        else
        {
            delete [] S;
            delete [] M;
        }

    }




















    // Summary
    if (!response)
    {
        printf("================================================================================\n");
        center_text("Summary");
        V = new double[n];
        F = new double[N];

        // Compute Total Dipole
        double Mtot[3] = {0., 0., 0.};
        total_dipole(M, Mtot, n);

        // Compute Energies
        E_electrostatic = electrostatic_energy(Q,V0,n);
        E_induction = induction_energy(M, S, n);
        E_total = E_electrostatic + E_induction;

        // Print data
        printf("Electrostatic Energy (eV): \t %20.8f\n", E_electrostatic);
        printf("Induction Energy (eV): \t\t %20.8f\n\n", E_induction);
        printf("Total Energy (eV): \t\t %20.8f\n", E_total);
        printf("Total Induced Dipole (eÅ): \t %20.8f %12.8f %12.8f\n\n", Mtot[0], Mtot[1], Mtot[2]);

        // If PBC are enabled, print Zeta
        if (PBC && any_of(F_ext, F_ext+3, [](double x) {return x!=0.;}))
        {
            double Vol = volume(PBC_box);
            double Field = cblas_dnrm2(3,F_ext,1);
            double* Z = new double[3]{0.};
            double* zeros = new double[9]{0.};

            risposta(Mtot,depol,Vol,Field,Z);
            printf("Zeta (unitless): \t\t %20.8f %12.8f %12.8f\n\n", Z[0],Z[1],Z[2]);

            delete [] Z;
            delete [] zeros;
        }

        // Compute final potentials and fields
        printf("Saving final potentials and fields to potential_field.dat\n");
        if (PBC_radius == 0.)
        {
            potential_field(X, M, V0, F0, V, F, epsilon, n);
        }
        else
        {
            potential_field(X, M, V0, F0, V, F, PBC_radius, PBC_box, epsilon, n);
        }
        save_potential_field("potential_field.dat", V, F, n);

        // Save output, potentials and fields to file
        printf("Saving results to %s.dat\n",name.c_str());
        info = write_output(name, X, Q, M, n);


        // Deallocate
        delete [] X;
        delete [] Q;
        delete [] S;
        delete [] M;
        delete [] V0;
        delete [] F0;
        delete [] V;
        delete [] F;

    }

    t2 = omp_get_wtime();
    printf("\nTime (s): %-10.3f\n",t2-t1);

    if (info != 0)
    {
        exit(info);
    }


    // Exit
    printf("================================================================================\n");
    double total_time2 = omp_get_wtime();
    printf("\nTotal time (s): %-10.3f\n\n",total_time2-total_time1);

    center_text("SUCCESS: Program returned normally !");
    printf("================================================================================\n");

    return 0;
}
