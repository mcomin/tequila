/*
Tequila: Micro-Electrostatics Software
Copyright (C) 2021  Massimiliano Comin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact: https://gitlab.com/mcomin/tequila/
*/
#include <cmath>
#include <iostream>
#include <algorithm>
#include <array>
#include <cblas.h>
#include <omp.h>
#include "module_io.h"
#include "module_math.h"

// Include lapacke only if compiled with GNU
#ifndef __INTEL_COMPILER
#include <lapacke.h>
#endif


const double ke = 14.399645480613911; // [eV Å / e^2]
const double pi = 3.1415926535897932;
const double eps0 = 1. / (4. * pi * ke);
const char uplo = 'L';
const char jobz = 'V';


// Normal: M is a deep copy of S. Zeta: M is a deep copy of (Sx,Sy,Sz). //
int inverse(double* T, double* M, const int& N, const int& NRHS)
{
    int info;
    int count = 0;
    int* IPIV = new int[N];

    // Solve T.M = S
    info = LAPACKE_dsysv(LAPACK_COL_MAJOR, uplo, N, NRHS, T, N, IPIV, M, N);

    // Check T is Positive-Definite
    for (int i=0; i<N; ++i)
    {
        // 2x2 Block in T: for T to be positive definite, all 1x1 and 2x2 blocks must be positive definite
        if (IPIV[i] < 0. && (IPIV[i] == IPIV[i+1]))
        {
            double det = T[N*i+i]*T[N*(i+1)+i+1] - T[N*(i+1)+i]*T[N*i+i+1];
            double trace = T[N*i+i] + T[N*(i+1)+i+1];
            if ((det <= 0.) || (trace <= 0.)) // 2x2 Block is pos-def iff det > 0. && trace > 0.
            {
                count += 1;
            }
        }
        else
        {
            if (T[N*i+i] <= 0.)
            {
                count += 1;
            }
        }
    }

    if (count == 0)
    {
        printf("\n=> Interaction Matrix is positive definite!\n" );
    }
    else
    {
        printf("\n=> WARNING: Interaction Matrix is not positive definite!\n");
    }
    printf("Instabilities: %d\n",count);

    if (info < 0)
    {
        printf("FATAL ERROR: Solver (dsysv) failed: illegal argument %d\n",-info);
    }

    if (info > 0)
    {
        printf("FATAL ERROR: Solver (dsysv) failed: diagonal element %d = 0, no solution could be found\n",info);
    }

    delete [] IPIV;
    return info;
}





// Compute eigensystem of T (NxN): Store eigenvalues in W and eigenvectors in T
int eigen(double* T, double* W, const int& N)
{
    int info;

    // Eigenvalues will be in W, Eigenvectors in T
    info = LAPACKE_dsyev(LAPACK_COL_MAJOR, jobz, uplo, N, T, N, W);

    if (info < 0)
    {
        printf("FATAL ERROR: Eigensolver (dsyev) failed: illegal argument %d\n",-info);
    }
    else if (info > 0)
    {
        printf("FATAL ERROR: Eigensolver (dsyev) failed: failed to converge %d\n",info);
    }

    return info;
}














/*
Compute Zeta or Chi Vector
--------------------------
P/eps0 = Chi.Ftot = Zeta.Fext
Ftot = Fext - P.D/eps0

P       (3, vector)                Total Induced Dipole
D       (9, matrix)                Depolarization tensor (Can be 0)
V       (1, scalar)                Simulation Box Volume
F       (1, scalar)                External Applied Field norm
Z       (3, vector)                Zeta vector
*/
void risposta(const double* P, const double* D, const double& V, const double& F, double* Z)
{

    // Compute P.D/eps0
    double Depolarization[3];
    cblas_dgemv(CblasRowMajor,CblasNoTrans,3,3,1.,D,3,P,1,0.,Depolarization,1);

    for (int i=0; i<3; ++i)
    {
        Z[i] = P[i] / (V * eps0 * F - Depolarization[i]);
    }

}











/*
Invert linear system from eigendecomposition
--------------------------------------------
M = V.D^-1.V^T.S

V is the matrix of eigenvectors (= T here)
W is the vector of eigenvalues
S is the source vector
M is a N*3 vector of induced dipoles
*/
void solve(const double* T, const double* W, const double* S, double* M, const int& N)
{

    double* sum = new double[N]{0.};

    cblas_dgemv(CblasColMajor,CblasTrans,N,N,1.,T,N,S,1,0.,sum,1);

    #pragma omp parallel for simd
    for (int i=0; i<N; ++i)
    {
        sum[i] /= W[i];
    }

    cblas_dgemv(CblasColMajor,CblasNoTrans,N,N,1.,T,N,sum,1,0.,M,1);

    delete [] sum;
}





/*
Compute the sum of eigenvectors along X,Y,Z
--------------------------------------------
M = V.D^-1.V^T.S

T is the matrix of eigenvectors
R is a 3*N vector of eigenvector sums along X,Y,Z
*/
void eigenvector_sum(const double* T, double* R, const int& n)
{

    int N = 3*n;
    double sum;

    for (int i=0; i<3; ++i)
    {
        for (int j=0; j<N; ++j)
        {
            sum = 0.;

            #pragma omp parallel for simd reduction(+:sum)
            for (int k=0; k<n; ++k)
            {
                sum += T[N*j+3*k+i];
            }

             R[j+N*i] = sum;
        }
    }

}





/*
Compute Zeta from the eigenvector sums (eigvecsum) and eigenvalues
------------------------------------------------------------------
Z_xy = \sum_k R_k(xy) / lambda_k

R (3N) is the sum of eigenvectors along X,Y,Z
W (3N) are the eigenvalues
Z (3x3) is the Zeta matrix
V is the simulation cell volume
epsilon is the relative permittivity
*/
void spectral_zeta(const double* R, double* W, double* Z, const double& V, const double& epsilon, const int& N)
{

    // Compute R/W elementwise
    double* RW = new double[N*3];

    for (int i=0; i<3; ++i)
    {
        #pragma omp parallel for simd
        for (int j=0; j<N; ++j)
        {

            if (W[j] > 0.)
            {
                RW[j+N*i] = R[j+N*i] / W[j];
            }
            else
            {
                RW[j+N*i] = 0.;
            }

        }
    }

    inverse_zeta(R, RW, Z, V, epsilon, N);

    delete [] RW;

}




/*
Compute Zeta from the sum of the inverse Hessian
------------------------------------------------
Z_xy = \sum H^{-1} along x,y

R (3N) is eiher the sum of H^{-1} along X,Y,Z or the sum of eigenvectors along X,Y,Z
S (3N) is eiher the sum of H^{-1} along X,Y,Z or the sum of eigenvectors along X,Y,Z divided by the eigenvalues
Z (3x3) is the Zeta matrix
V is the simulation cell volume
epsilon is the relative permittivity
*/
void inverse_zeta(const double* R, double* S, double* Z, const double& V, const double& epsilon, const int& N)
{

    // Compute dot products
    Z[0] = epsilon * cblas_ddot(N, R, 1, S, 1)         / (eps0 * V);   // xx
    Z[1] = epsilon * cblas_ddot(N, R, 1, S+N, 1)       / (eps0 * V);   // xy
    Z[2] = epsilon * cblas_ddot(N, R, 1, S+2*N, 1)     / (eps0 * V);   // xz
    Z[3] = Z[1];                                                       // yx
    Z[4] = epsilon * cblas_ddot(N, R+N, 1, S+N, 1)     / (eps0 * V);   // yy
    Z[5] = epsilon * cblas_ddot(N, R+N, 1, S+2*N, 1)   / (eps0 * V);   // yz
    Z[6] = Z[2];                                                       // zx
    Z[7] = Z[5];                                                       // zy
    Z[8] = epsilon * cblas_ddot(N, R+2*N, 1, S+2*N, 1) / (eps0 * V);   // zz

}




/*
Inverse of a 3x3 matrix (analytic)
----------------------------------
M is the matrix to be inverted
Inv is the computed inverse
*/
void matrix_inverse_3(const double* M, double* Inv)
{
    // Compute determinant
    double det =  M[0] * (M[4]*M[8] - M[7]*M[5])
                + M[1] * (M[6]*M[5] - M[3]*M[8])
                + M[2] * (M[3]*M[7] - M[6]*M[4]);

    // Compute inverse
    Inv[0] = (M[4]*M[8] - M[7]*M[5]) / det; // xx
    Inv[1] = (M[2]*M[7] - M[1]*M[8]) / det; // xy
    Inv[2] = (M[1]*M[5] - M[4]*M[2]) / det; // yx
    Inv[3] = (M[5]*M[6] - M[8]*M[3]) / det; // xz
    Inv[4] = (M[0]*M[8] - M[2]*M[6]) / det; // zx
    Inv[5] = (M[2]*M[3] - M[0]*M[5]) / det; // yy
    Inv[6] = (M[3]*M[7] - M[4]*M[6]) / det; // yz
    Inv[7] = (M[1]*M[6] - M[0]*M[7]) / det; // zy
    Inv[8] = (M[0]*M[4] - M[1]*M[3]) / det; // zz
}





/*
Compute Chi from the depolarization correction to Zeta
------------------------------------------------------
Z (3x3) is the Zeta matrix
D (3x3) is the depolarization tensor
C (3x3) is the resulting Chi matrix
*/
void depolarization(const double* Z, const double* D, double* C)
{

    // Compute D.Z
    cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,3,3,3,1.,D,3,Z,3,0.,C,3);

    // D.Z -> 1 - D.Z
    for (int i=0; i<9; ++i)
    {
        if ((i==0)||(i==4)||(i==8))
        {
            C[i] = 1. - C[i];
        }
        else
        {
            C[i] = -C[i];
        }
    }

    // Compute inv(1 - D.Z)
    double* A = new double[9]{0.};
    matrix_inverse_3(C,A);

    // Compute inv(1 - D.Z) . Z
    cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,3,3,3,1.,A,3,Z,3,0.,C,3);

    delete [] A;

}




















double volume(const double* PBC_box)
{
    return    PBC_box[0] * (PBC_box[4]*PBC_box[8] - PBC_box[7]*PBC_box[5])
            + PBC_box[1] * (PBC_box[6]*PBC_box[5] - PBC_box[3]*PBC_box[8])
            + PBC_box[2] * (PBC_box[3]*PBC_box[7] - PBC_box[6]*PBC_box[4]);
}




inline void distance(const double* X, double* rij, double& r, const int& i, const int& j)
{
    r = 0.;

    #pragma omp simd
    for (int k=0; k<3; k++)
    {
        rij[k] = X[j*3+k] - X[i*3+k];
        r += pow(rij[k],2);
    }

    r = sqrt(r);
}


inline void distance(const double* X, const double* v, double* rij, double& r, const int& i, const int& j)
{
    r = 0.;

    #pragma omp simd
    for (int k=0; k<3; k++)
    {
        rij[k] = X[j*3+k] - X[i*3+k] + v[k];
        r += pow(rij[k],2);
    }

    r = sqrt(r);
}







/*
Fill the diagonal block (i)
---------------------------

T is the interaction matrix
P is the vector of polarizability tensors
*/
inline void diagonal_block(double* T, const double* P, const int& m, const int& n, const int& i)
{
    if (m == 10)
    {
        // Compute determinant
        double det =  P[0+6*i] * (P[3+6*i]*P[5+6*i] - P[4+6*i]*P[4+6*i])
                    + P[1+6*i] * (P[2+6*i]*P[4+6*i] - P[1+6*i]*P[5+6*i])
                    + P[2+6*i] * (P[1+6*i]*P[4+6*i] - P[2+6*i]*P[3+6*i]);

        det /= ke;

        // Set 3x3 submatrix (i,i) to inv(alpha)
        T[(3*i+0)*3*n+3*i+0] += (P[3+6*i]*P[5+6*i] - P[4+6*i]*P[4+6*i]) / det; // xx
        T[(3*i+0)*3*n+3*i+1] += (P[2+6*i]*P[4+6*i] - P[1+6*i]*P[5+6*i]) / det; // xy
        T[(3*i+1)*3*n+3*i+0] += (P[2+6*i]*P[4+6*i] - P[1+6*i]*P[5+6*i]) / det; // yx
        T[(3*i+0)*3*n+3*i+2] += (P[1+6*i]*P[4+6*i] - P[2+6*i]*P[3+6*i]) / det; // xz
        T[(3*i+2)*3*n+3*i+0] += (P[1+6*i]*P[4+6*i] - P[2+6*i]*P[3+6*i]) / det; // zx
        T[(3*i+1)*3*n+3*i+1] += (P[0+6*i]*P[5+6*i] - P[2+6*i]*P[2+6*i]) / det; // yy
        T[(3*i+1)*3*n+3*i+2] += (P[1+6*i]*P[2+6*i] - P[0+6*i]*P[4+6*i]) / det; // yz
        T[(3*i+2)*3*n+3*i+1] += (P[1+6*i]*P[2+6*i] - P[0+6*i]*P[4+6*i]) / det; // zy
        T[(3*i+2)*3*n+3*i+2] += (P[0+6*i]*P[3+6*i] - P[1+6*i]*P[1+6*i]) / det; // zz
    }
    else
    {
        // Set 3x3 submatrix (i,i) to Trace 1/alpha
        T[(3*i+0)*3*n+3*i+0] += ke / P[i]; // xx
        T[(3*i+0)*3*n+3*i+1] += 0.; // xy
        T[(3*i+1)*3*n+3*i+0] += 0.; // yx
        T[(3*i+0)*3*n+3*i+2] += 0.; // xz
        T[(3*i+2)*3*n+3*i+0] += 0.; // zx
        T[(3*i+1)*3*n+3*i+1] += ke / P[i]; // yy
        T[(3*i+1)*3*n+3*i+2] += 0.; // yz
        T[(3*i+2)*3*n+3*i+1] += 0.; // zy
        T[(3*i+2)*3*n+3*i+2] += ke / P[i]; // zz
    }

}



/*
Dipole-Dipole interaction element (k,l)
---------------------------------------

*/
inline double dipole_dipole(const double* rij, const double& r, const double& epsilon,
                            const int& k, const int& l)
{
    if (k == l)
    {
        return ke / (epsilon * pow(r,3)) * (1. - 3. * rij[k] * rij[l] / pow(r,2));
    }
    else
    {
        return - 3. * ke * rij[k] * rij[l] / (epsilon * pow(r,5));
    }
}


// Dipole-Dipole interaction with Tinker exponential screening
// u is u^3 as computed by tinker_range_parameter
inline double dipole_dipole_tinker(const double* rij, const double& r, const double& u, const double& epsilon,
                                   const int& k, const int& l)
{
    double f = 1. - exp(-u);
    double g = 1. - (1. + u) * exp(-u);

   if (k == l)
   {
       return ke / (epsilon * pow(r,3)) * (f - 3. * g * rij[k] * rij[l] / pow(r,2));
   }
   else
   {
       return - 3. * g * ke * rij[k] * rij[l] / (epsilon * pow(r,5));
   }
}


// Compute the range parameter u^3
inline double tinker_range_parameter(const double* P, const double& r, const int& i, const int& j,
                                     const int& m, const int& n)
{
    if (m == 10)
    {
        double trace_i = (P[0+6*i] + P[3+6*i] + P[5+6*i]) / 3.;
        double trace_j = (P[0+6*j] + P[3+6*j] + P[5+6*j]) / 3.;
        return pow(r,3) / sqrt(trace_i * trace_j);
    }
    else
    {
        return pow(r,3) / sqrt(P[i] * P[j]);
    }

}











double electrostatic_energy(const double* Q, const double* V0, const int& n)
{
    return 0.5 * cblas_ddot(n,Q,1,V0,1);
}

double induction_energy(const double* M, const double* S, const int& n)
{
    return - 0.5 * cblas_ddot(3*n,M,1,S,1);
}


/* Total Dipole in eÅ */
void total_dipole(const double* M, double* D, const int& n)
{
    #pragma omp parallel for reduction(+:D[:3])
    for (int i=0; i<n; ++i)
    {
        D[0] += M[0+3*i];
        D[1] += M[1+3*i];
        D[2] += M[2+3*i];
    }
}






















/*
INITIAL Potentials and Fields
-----------------------------
V0_i = sum_j ke q_j / r_ij        (i != j)
F0_i = - sum_j ke q_j r_ij / r_ij^3        (i != j)
*/
void potential_field(const double* X, const double* Q, const double* F, double* V0, double* F0,
                     const double& epsilon, const int& n)
{
    double rij[3];
    double r;

    #pragma omp parallel for private(rij,r)
    for (int i=0; i<n; ++i)
    {
        for (int j=0; j<n; ++j)
        {
            if (i != j)
            {
                // Compute separation rij & distance r
                distance(X,rij,r,i,j);

                // Compute potential on site i
                V0[i] += ke * Q[j] / (epsilon * r);

                // Compute field on site i
                for (int k=0; k<3; ++k)
                {
                    F0[3*i+k] -= ke * Q[j] * rij[k] / (epsilon * pow(r,3));
                }
            }
        }


        // Add External Field to potential on site i
        V0[i] -= cblas_ddot(3,F,1,X+3*i,1) / epsilon;

        // Add External Field to field on site i
        for (int k=0; k<3; ++k)
        {
            F0[3*i+k] += F[k] / epsilon;
        }
    }
}

/*
INITIAL Potentials and Fields       (with PBC)
-----------------------------
V0_i = sum_j ke q_j / r_ij        (i != j)
F0_i = - sum_j ke q_j r_ij / r_ij^3        (i != j)
*/
void potential_field(const double* X, const double* Q, const double* F, double* V0, double* F0,
                     const double& PBC_radius, const double* PBC_box, const double& epsilon, const int& n)
{
    // Temporary variables
    double r,v;
    double rij[3], vect[3];

   // Compute number of cells within radius
   double PBC_norm[3];

   for (int k=0; k<3; k++)
   {
       PBC_norm[k] = cblas_dnrm2(3,PBC_box+3*k,1);
   }

   int PBC_n[3] = {(int)ceil(PBC_radius/PBC_norm[0])+1,
                   (int)ceil(PBC_radius/PBC_norm[1])+1,
                   (int)ceil(PBC_radius/PBC_norm[2])+1};

   #pragma omp parallel for private(rij,r,vect,v) schedule(dynamic)
   for (int i=0; i<n; ++i)
   {
       // Loop though PBC replica
       for (int n1=-PBC_n[0]; n1<=PBC_n[0]; ++n1)
       {
           for (int n2=-PBC_n[1]; n2<=PBC_n[1]; ++n2)
           {
               for (int n3=-PBC_n[2]; n3<=PBC_n[2]; ++n3)
               {
                   // Compute PBC translation vector & norm
                   v = 0.;
                   for (int k=0; k<3; ++k)
                   {
                       vect[k] = n1 * PBC_box[k] + n2 * PBC_box[k+3] + n3 * PBC_box[k+6];
                       v += pow(vect[k],2);
                   }
                   v = sqrt(v);

                   // Add periodic replica
                   if (v < PBC_radius)
                   {
                       // Loop through all other sites
                       for (int j=0; j<n; ++j)
                       {
                           // Field of j on i: (j != i) if physical j, no restriction for replica j
                           if (((i == j) && (n1 == 0) && (n2 == 0) && (n3 == 0)))
                           {
                               continue;
                           }
                           else
                           {
                               // Compute separation rij & distance r
                               distance(X, vect, rij, r, i, j);

                               // Compute potential on site i
                               V0[i] += ke * Q[j] / (epsilon * r);

                               // Compute field on site i
                               for (int k=0; k<3; ++k)
                               {
                                   F0[3*i+k] -= ke * Q[j] * rij[k] / (epsilon * pow(r,3));
                               }
                           }

                       }

                       // Add External Field to potential on site i
                       V0[i] -= cblas_ddot(3,F,1,X+3*i,1) / epsilon;

                       // Add External Field to field on site i
                       for (int k=0; k<3; ++k)
                       {
                           F0[3*i+k] += F[k] / epsilon;
                       }
                   }

               }
           }
       }

   }
}





/*
FINAL Potentials and Fields
---------------------------
V_i = V0_i + sum_j ke mu_j . r_ij / r_ij^3      (i != j)
F_i = F0_i + sum_j ke (3 mu_j . r_ij / r_ij^3) - mu_j / r_ij^3        (i != j)
*/
void potential_field(const double* X, const double* M, const double* V0, const double* F0, double* V, double* F,
                     const double& epsilon, const int& n)
{
    double rij[3];
    double r,x;

    #pragma omp parallel for private(rij,r,x)
    for (int i=0; i<n; ++i)
    {
        // Initialize
        V[i] = V0[i];
        for (int k=0; k<3; ++k)
        {
            F[3*i+k] = F0[3*i+k];
        }

        for (int j=0; j<n; ++j)
        {
            if (i != j)
            {
                // Compute separation rij & distance r
                distance(X,rij,r,i,j);

                // Compute dot product of dipole and separation
                x = cblas_ddot(3,M+3*j,1,rij,1);

                // Update potential on site i
                V[i] -= ke * x / (epsilon * pow(r,3));

                // Update field on site i
                for (int k=0; k<3; ++k)
                {
                    F[3*i+k] += ke * (3. * x * rij[k] / (epsilon * pow(r,5)) - M[3*j+k] / (epsilon * pow(r,3)));
                }
            }
        }
    }
}


/*
FINAL Potentials and Fields         (with PBC)
---------------------------
V_i = V0_i + sum_j ke mu_j . r_ij / r_ij^3      (i != j)
F_i = F0_i + sum_j ke (3 mu_j . r_ij / r_ij^3) - mu_j / r_ij^3        (i != j)
*/
void potential_field(const double* X, const double* M, const double* V0, const double* F0, double* V, double* F,
                     const double& PBC_radius, const double* PBC_box, const double& epsilon, const int& n)
{
    // Temporary variables
    double r,v,x;
    double rij[3], vect[3];

   // Compute number of cells within radius
   double PBC_norm[3];

   for (int k=0; k<3; k++)
   {
       PBC_norm[k] = cblas_dnrm2(3,PBC_box+3*k,1);
   }

   int PBC_n[3] = {(int)ceil(PBC_radius/PBC_norm[0])+1,
                   (int)ceil(PBC_radius/PBC_norm[1])+1,
                   (int)ceil(PBC_radius/PBC_norm[2])+1};

   #pragma omp parallel for private(rij,r,vect,v,x) schedule(dynamic)
   for (int i=0; i<n; ++i)
   {
       // Initialize
       V[i] = V0[i];
       for (int k=0; k<3; ++k)
       {
           F[3*i+k] = F0[3*i+k];
       }

       // Loop though PBC replica
       for (int n1=-PBC_n[0]; n1<=PBC_n[0]; ++n1)
       {
           for (int n2=-PBC_n[1]; n2<=PBC_n[1]; ++n2)
           {
               for (int n3=-PBC_n[2]; n3<=PBC_n[2]; ++n3)
               {
                   // Compute PBC translation vector & norm
                   v = 0.;
                   for (int k=0; k<3; ++k)
                   {
                       vect[k] = n1 * PBC_box[k] + n2 * PBC_box[k+3] + n3 * PBC_box[k+6];
                       v += pow(vect[k],2);
                   }
                   v = sqrt(v);

                   // Add periodic replica
                   if (v < PBC_radius)
                   {
                       // Loop through all other sites
                       for (int j=0; j<n; ++j)
                       {
                           // Field of j on i: (j != i) if physical j, no restriction for replica j
                           if (((i == j) && (n1 == 0) && (n2 == 0) && (n3 == 0)))
                           {
                               continue;
                           }
                           else
                           {
                               // Compute separation rij & distance r
                               distance(X, vect, rij, r, i, j);

                               // Compute dot product of dipole and separation
                               x = cblas_ddot(3,M+3*j,1,rij,1);

                               // Update potential on site i
                               V[i] -= ke * x / (epsilon * pow(r,3));

                               // Update field on site i
                               for (int k=0; k<3; ++k)
                               {
                                   F[3*i+k] += ke * (3. * x * rij[k] / (epsilon * pow(r,5)) - M[3*j+k] / (epsilon * pow(r,3)));
                               }
                           }

                       }
                   }

               }
           }
       }

   }
}










/*
FINAL Potentials and Fields for ZETA calculation

M is a 3N*3 vector of induced dipoles for fields in x,y,z (column-major)
PBC_box is a 9 vector of 3 cell vectors (row-major)
field is the external applied field
*/
void potential_field_zeta(const double* X, const double* M, const double& field, double* V, double* F,
                          const double& PBC_radius, const double* PBC_box, const double& epsilon,
                          const int& n, const int& direction)
{
    // Temporary variables
    double r,v,x;
    double rij[3], vect[3];

   // Compute number of cells within radius
   double PBC_norm[3];

   for (int k=0; k<3; k++)
   {
       PBC_norm[k] = cblas_dnrm2(3,PBC_box+3*k,1);
   }

   int PBC_n[3] = {(int)ceil(PBC_radius/PBC_norm[0])+1,
                   (int)ceil(PBC_radius/PBC_norm[1])+1,
                   (int)ceil(PBC_radius/PBC_norm[2])+1};

   #pragma omp parallel for private(rij,r,vect,v,x) schedule(dynamic)
   for (int i=0; i<n; ++i)
   {
       // Initialize
       V[i] = - field * X[3*i+direction] / epsilon;
       for (int k=0; k<3; ++k)
       {
           if (k == direction)
           {
               F[3*i+k] = field / epsilon;
           }
           else
           {
               F[3*i+k] = 0.;
           }
       }


       // Loop though PBC replica
       for (int n1=-PBC_n[0]; n1<=PBC_n[0]; ++n1)
       {
           for (int n2=-PBC_n[1]; n2<=PBC_n[1]; ++n2)
           {
               for (int n3=-PBC_n[2]; n3<=PBC_n[2]; ++n3)
               {
                   // Compute PBC translation vector & norm
                   v = 0.;
                   for (int k=0; k<3; ++k)
                   {
                       vect[k] = n1 * PBC_box[k] + n2 * PBC_box[k+3] + n3 * PBC_box[k+6];
                       v += pow(vect[k],2);
                   }
                   v = sqrt(v);

                   // Add periodic replica
                   if (v < PBC_radius)
                   {

                       // Loop through all other sites
                       for (int j=0; j<n; ++j)
                       {
                           // Field of j on i: (j != i) if physical j, no restriction for replica j
                           if (((i == j) && (n1 == 0) && (n2 == 0) && (n3 == 0)))
                           {
                               continue;
                           }
                           else
                           {
                               // Compute separation rij & distance r
                               distance(X, vect, rij, r, i, j);

                               // Compute dot product of dipole and separation
                               x = cblas_ddot(3,M+3*j,1,rij,1);

                               // Update potential on site i
                               V[i] -= ke * x / (epsilon * pow(r,3));

                               // Update field on site i
                               for (int k=0; k<3; ++k)
                               {
                                   F[3*i+k] += ke * (3. * x * rij[k] / (epsilon * pow(r,5)) - M[3*j+k] / (epsilon * pow(r,3)));
                               }
                           }

                       }
                   }

               }
           }
       }

   }
}


























// Interaction Matrix in V/Å^2
void interaction_matrix(const double* X, const double* P, double* T,
                        const double& epsilon, const int& m, const int& n, const double& hack)
{
    // Temporary variables
    double x,r;
    double rij[3];

    // Fill Interaction Matrix
    #pragma omp parallel for private(rij,r,x)
    for (int i=0; i<n; ++i)
    {
        // Fill Diagonal Entries
        diagonal_block(T, P, m, n, i);

        // Loop through all other sites
        for (int j=0; j<i; ++j)
        {
            // Compute rij and norm(rij)
            distance(X, rij, r, i, j);

            // Off-diagonal
            for (int k=0; k<3; ++k)
            {
                for (int l=0; l<=k; ++l)
                {
                    if ((P[i] != P[j]) && (r < hack))
                    {
                        x = 0.;
                    }
                    else
                    {
                        x = dipole_dipole(rij, r, epsilon, k, l);
                    }

                    T[(3*j+l)*3*n+3*i+k] = x;
                    T[(3*i+l)*3*n+3*j+k] = x;
                    T[(3*j+k)*3*n+3*i+l] = x;
                    T[(3*i+k)*3*n+3*j+l] = x;

                }
            }


        }
    }
}




// Interaction Matrix with PBC in V/Å^2
void interaction_matrix(const double* X, const double* P, double* T,
                        const double& PBC_radius, const double* PBC_box,
                        const double& epsilon, const int& m, const int& n, const double& hack)
{
    // Temporary variables
    double x;
    double r, v;
    double rij[3], vect[3], PBC_norm[3];;
    long long int counter = 0;

    // Compute number of cells within radius
    for (int k=0; k<3; k++)
    {
        PBC_norm[k] = cblas_dnrm2(3,PBC_box+3*k,1);
    }

    int PBC_n[3] = {(int)ceil(PBC_radius/PBC_norm[0])+1,
                    (int)ceil(PBC_radius/PBC_norm[1])+1,
                    (int)ceil(PBC_radius/PBC_norm[2])+1};


    // Fill Interaction Matrix
    #pragma omp parallel for private(vect,v,rij,r,x) reduction(+:counter) schedule(dynamic)
    for (int i=0; i<n; ++i)
    {
        // Fill Diagonal Entries
        diagonal_block(T, P, m, n, i);


        // Loop over 3 PBC vectors
        for (int n1=-PBC_n[0]; n1<=PBC_n[0]; ++n1)
        {
            for (int n2=-PBC_n[1]; n2<=PBC_n[1]; ++n2)
            {
                for (int n3=-PBC_n[2]; n3<=PBC_n[2]; ++n3)
                {

                    // Compute PBC translation vector & norm
                    v = 0.;
                    for (int k=0; k<3; ++k)
                    {
                        vect[k] = n1 * PBC_box[k] + n2 * PBC_box[k+3] + n3 * PBC_box[k+6];
                        v += pow(vect[k],2);
                    }
                    v = sqrt(v);

                    if (v < PBC_radius)
                    {
                        // Interaction of physical dipole i with dipole j
                        for (int j=0; j<n; ++j)
                        {

                            // We take (i == j) only if j is a replica of i
                            if ( (i == j) && ((n1 != 0) || (n2 != 0) || (n3 != 0)) )
                            {

                                ++counter;

                                // 3x3 submatrix: dipole-dipole interaction (when i==j, rij=vect, r=v)
                                for (int k=0; k<3; ++k)
                                {
                                    for (int l=0; l<3; ++l)
                                    {
                                        if ((P[i] != P[j]) && (v < hack))
                                        {
                                            x = 0.;
                                        }
                                        else
                                        {
                                            x = dipole_dipole(vect, v, epsilon, k, l);
                                        }

                                        T[(3*i+k)*3*n+3*i+l] += x;
                                    }
                                }


                            }
                            // We take all (i != j)
                            if (i != j)
                            {
                                // Compute separation rij & distance r
                                distance(X, vect, rij, r, i, j);

                                // 3x3 submatrix: dipole-dipole interaction
                                for (int k=0; k<3; ++k)
                                {
                                    for (int l=0; l<3; ++l)
                                    {
                                        if ((P[i] != P[j]) && (r < hack))
                                        {
                                            x = 0.;
                                        }
                                        else
                                        {
                                            x = dipole_dipole(rij, r, epsilon, k, l);
                                        }

                                        T[(3*i+k)*3*n+3*j+l] += x;
                                    }
                                }

                            }

                        }
                    }


                }
            }
        }
    }
    printf("Total number of PBC replica: %lld\n",counter);
}
























// Source vector in V/Å
void source_vector(const double* X, const double* Q, const bool* B, double* S, double* V0, double* F0,
                   const double& epsilon, const double* F, const int& n)
{
    // Temporary variables
    double r;
    double rij[3];

    if (any_of(B, B+n, [](bool x) {return x;}))
    {
        #pragma omp parallel for private(rij,r)
        for (int i=0; i<n; ++i)
        {
            // Loop through all other sites
            for (int j=0; j<n; ++j)
            {
                if ((i != j) && B[j])
                {
                    // Compute separation rij & distance r
                    distance(X, rij, r, i, j);

                    // Field of permanent charges on dipole
                    for (int k=0; k<3; ++k)
                    {
                        S[3*i+k] -= ke * Q[j] * rij[k] / (epsilon * pow(r,3));
                    }

                    // Compute potential on site i
                    V0[i] += ke * Q[j] / (epsilon * r);
                }
            }
        }
    }

    if (!(F[0] == 0. && F[1] == 0. && F[2] == 0.))
    {
        printf("Adding uniform external field!\n");
    }

    #pragma omp parallel for
    for (int i=0; i<n; ++i)
    {
        for (int k=0; k<3; ++k)
        {
            F0[3*i+k] = S[3*i+k] + F[k] / epsilon;
            S[3*i+k] += F[k] / epsilon;
        }

        // Add External field to potential at site i
        V0[i] -= cblas_ddot(3,F,1,X+3*i,1) / epsilon;
    }
}




// Source vector with PBC in V/Å
void source_vector(const double* X, const double* Q, const bool* B, double* S, double* V0, double* F0,
                   const double& PBC_radius, const double* PBC_box,
                   const double& epsilon, const double* F, const int& n)
{
    // Temporary variables
    double r,v;
    double rij[3], vect[3];
    long long int counter = 0;

    if (any_of(B, B+n, [](bool x) {return x;}))
    {
        // Compute number of cells within radius
        double PBC_norm[3];

        for (int k=0; k<3; k++)
        {
            PBC_norm[k] = cblas_dnrm2(3,PBC_box + 3*k,1);
        }

        int PBC_n[3] = {(int)ceil(PBC_radius/PBC_norm[0])+1,
                        (int)ceil(PBC_radius/PBC_norm[1])+1,
                        (int)ceil(PBC_radius/PBC_norm[2])+1};

        #pragma omp parallel for private(rij,r,vect,v) reduction(+:counter) schedule(dynamic)
        for (int i=0; i<n; ++i)
        {
            // Loop though PBC replica
            for (int n1=-PBC_n[0]; n1<=PBC_n[0]; ++n1)
            {
                for (int n2=-PBC_n[1]; n2<=PBC_n[1]; ++n2)
                {
                    for (int n3=-PBC_n[2]; n3<=PBC_n[2]; ++n3)
                    {
                        // Compute PBC translation vector & norm
                        v = 0.;
                        for (int k=0; k<3; k++)
                        {
                            vect[k] = n1 * PBC_box[k] + n2 * PBC_box[k+3] + n3 * PBC_box[k+6];
                            v += pow(vect[k],2);
                        }
                        v = sqrt(v);

                        // Add periodic replica
                        if (v < PBC_radius)
                        {
                            ++counter;

                            // Loop through all other sites
                            for (int j=0; j<n; ++j)
                            {
                                // Field of j on i: (j != i) if physical j, no restriction for replica j
                                if (((i == j) && (n1 == 0) && (n2 == 0) && (n3 == 0)))
                                {
                                    continue;
                                }
                                else if (B[j])
                                {
                                    // Compute separation rij & distance r
                                    distance(X, vect, rij, r, i, j);

                                    // Field of permanent charges on dipole
                                    for (int k=0; k<3; ++k)
                                    {
                                        S[3*i+k] -= ke * Q[j] * rij[k] / (epsilon * pow(r,3));
                                    }

                                    // Compute potential on site i
                                    V0[i] += ke * Q[j] / (epsilon * r);
                                }

                            }
                        }

                    }
                }

            }

        }
    }


    if (!(F[0] == 0. && F[1] == 0. && F[2] == 0.))
    {
        printf("Adding uniform external field!\n");
    }

    #pragma omp parallel for
    for (int i=0; i<n; ++i)
    {
        // Add External field to Field and Source vector
        for (int k=0; k<3; ++k)
        {
            F0[3*i+k] = S[3*i+k] + F[k] / epsilon;
            S[3*i+k] += F[k] / epsilon;
        }

        // Add External field to potential at site i
        V0[i] -= cblas_ddot(3,F,1,X+3*i,1) / epsilon;
    }

    if (any_of(B, B+n, [](bool x) {return x;}))
    {
        printf("Total number of PBC replica: %lld\n",counter-n);
    }
}
