#!/usr/bin/env python3
"""
Tequila: Micro-Electrostatics Software
Copyright (C) 2021  Massimiliano Comin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact: https://gitlab.com/mcomin/tequila/
"""
import os, sys, re
import subprocess as sp
from os.path import join, abspath, dirname

# Check version and imports
if sys.version[0] == '2':
    print('FATAL ERROR: Unit tests require /usr/bin/env/python3 to be a Python 3 executable.')
    print('Current Python version: ',sys.version)
    print('Modify the shebang of src/module_test.py to point to a valid Python 3 executable.')
    exit(0)

try:
    import numpy as np
except:
    print('FATAL ERROR: Cannot import NumPy')
    print('Current Python version: ',sys.version)
    print('Install NumPy for this Python version and run unit tests again.')
    exit(0)


# Define path & execution variables
bash = os.popen('command -v bash').read().rstrip()
root = dirname(dirname(abspath(__file__)))
tequila = join(root,'tequila')

def popen(cmd):
    return sp.check_output(cmd,shell=True,executable=bash).decode('unicode_escape')

def grep(expr, file):
    return popen(f"grep {expr} {file}")



# Test function
def test(name):
    "Compare Mescal & Tequila results"

    if 'zeta' in name:
        return test_zeta(name)

    path = join(root,'test',name)

    ### Check matrix inversion ###
    T = np.fromfile(join(path,'T.dat'), dtype=float)
    Q = np.fromfile(join(path,'S.dat'), dtype=float)
    T = T.reshape(Q.size,Q.size)
    I = np.linalg.inv(T)
    mu = I @ Q

    ### Read MESCal results ###
    truth = np.loadtxt(join(path,'mescal.rst'),skiprows=1)
    dipoles_true = truth[:,[7,8,9]] * 0.2081943 # (Debye -> eÅ)
    V_true = truth[:,10]
    F_true = truth[:,[11,12,13]]
    V0_true = truth[:,14]
    F0_true = truth[:,[15,16,17]]
    energies_true = np.loadtxt(join(path,'mescal_energy.dat'))


    ### Read Tequila data (eÅ) ###
    dipoles_tequila = np.loadtxt(join(path,'tequila.dat'),usecols=[4,5,6])
    potfield = np.loadtxt(join(path,'potential_field.dat'))
    potfield0 = np.loadtxt(join(path,'potential_field0.dat'))
    V_tequila = potfield[:,0]
    F_tequila = potfield[:,1:]
    V0_tequila = potfield0[:,0]
    F0_tequila = potfield0[:,1:]

    ### Read Tequila energies ###
    energies_tequila = np.zeros(3)
    with open(join(path,'tequila.out'),'r') as file:
        for line in file:
            if 'Total Energy' in line:
                energies_tequila[0] = float(line.split()[3])
            elif 'Electrostatic Energy' in line:
                energies_tequila[1] = float(line.split()[3])
            elif 'Induction Energy' in line:
                energies_tequila[2] = float(line.split()[3])


    ### Compare ###
    error = 0

    ### Compare eigvals & eigvecs for FULLDIAG calculation ###
    if 'fulldiag' in name:

        # True eigen
        w,v = np.linalg.eigh(T)

        # Read eigen
        eigvals = np.fromfile(join(path,'eigvals.dat'))
        eigvecs = np.fromfile(join(path,'eigvecs.dat')).reshape((eigvals.size,eigvals.size))
        eigvecsum_x = np.sum(eigvecs[:,0::3],axis=-1)
        eigvecsum_y = np.sum(eigvecs[:,1::3],axis=-1)
        eigvecsum_z = np.sum(eigvecs[:,2::3],axis=-1)

        eigvecsum_tequila =  np.fromfile(join(path,'eigvecsum.dat')).reshape(3,-1).T

        sum_condition = np.allclose(eigvecsum_tequila[:,0],eigvecsum_x)
        sum_condition = sum_condition and np.allclose(eigvecsum_tequila[:,1],eigvecsum_y)
        sum_condition = sum_condition and np.allclose(eigvecsum_tequila[:,2],eigvecsum_z)


        if np.allclose(w,eigvals.flatten()):
            print('Eigenvalues (1e-8): \t\t\t=> OK')
        else:
            print('Eigenvalues (1e-8): \t\t\t=> ERROR')
            error += 1

        # Compare eigenrecomposition
        rebuilt_T = eigvecs.T @ np.diag(eigvals) @ eigvecs

        if np.allclose(rebuilt_T,T):
            print('Eigenvectors (1e-8): \t\t\t=> OK')
        else:
            print('Eigenvectors (1e-8): \t\t\t=> ERROR')
            error += 1

        if sum_condition:
            print('Eigenvec Sum (1e-8): \t\t\t=> OK')
        else:
            print('Eigenvec Sum (1e-8): \t\t\t=> ERROR')
            error += 1


    if np.allclose(mu,dipoles_tequila.flatten()):
        print('Inversion wrt Eigen (1e-8): \t\t=> OK')
    else:
        print('Inversion wrt Eigen (1e-8): \t\t=> ERROR')
        error += 1

    if np.allclose(energies_true,energies_tequila,atol=1e-4):
        print('Total Energies (1e-4): \t\t\t=> OK')
    else:
        print('Total Energies (1e-4): \t\t\t=> ERROR')
        error += 1

    if np.allclose(V0_true,V0_tequila):
        print('Initial Potentials (1e-8): \t\t=> OK')
    else:
        print('Initial Potentials (1e-8): \t\t=> ERROR')
        error += 1

    if np.allclose(F0_true,F0_tequila):
        print('Initial Fields (1e-8): \t\t\t=> OK')
    else:
        print('Initial Fields (1e-8): \t\t\t=> ERROR')
        error += 1

    if np.allclose(V_true,V_tequila):
        print('Final Potentials (1e-8): \t\t=> OK')
    else:
        print('Final Potentials (1e-8): \t\t=> ERROR')
        error += 1

    if np.allclose(F_true,F_tequila):
        print('Final Fields (1e-8): \t\t\t=> OK')
    else:
        print('Final Fields (1e-8): \t\t\t=> ERROR')
        error += 1

    if np.allclose(dipoles_true,dipoles_tequila):
        print('Dipoles (1e-8): \t\t\t=> OK')
    else:
        print('Dipoles (1e-8): \t\t\t=> ERROR')
        error += 1

    if error > 0:
        print('\n=> ERROR: Unit test failed {} sub-tests !\n'.format(error))
    else:
        print('\n=> SUCCESS: Unit test passed !\n')

    return error




def test_zeta(name):

    path = join(root,'test',name)

    ### Read Zeta ###
    zeta_true = np.loadtxt(join(path,'mescal_zeta.dat'))
    zeta_tequila = np.loadtxt(join(path,'zeta.dat'))

    ### Compute Dielectric constant (Tequila) ###
    D = np.eye(3)/3
    inv = np.linalg.inv(np.eye(3) - D @ zeta_tequila)
    epsilon = np.trace(np.eye(3) +  zeta_tequila @ inv) / 3.

    ### Compute Dielectric constant (Mescal) ###
    D = np.eye(3)/3
    inv = np.linalg.inv(np.eye(3) - D @ zeta_true)
    epsilon_true = np.trace(np.eye(3) +  zeta_true @ inv) / 3.

    ### Compare ###
    error = 0

    #NOTE: Correcting zeta by hand (expected theoretically), since Mescal doesn't have the epsilon option
    if 'epsilon' in name:
        zeta_true /= 3.
        epsilon_true = np.trace(np.eye(3) +  zeta_true @ inv) / 3.

    if np.allclose(zeta_true,zeta_tequila):
        print('Zeta (1e-8): \t\t\t\t=> OK')
    else:
        print('Zeta (1e-8): \t\t\t\t=> ERROR')
        error += 1

    if np.allclose(epsilon_true,epsilon):
        print('Clausius-Mossotti (1e-8): \t\t=> OK')
    else:
        print('Clausius-Mossotti (1e-8): \t\t=> ERROR')
        error += 1

    if 'fulldiag' in name:
        chi_tequila = np.loadtxt(join(path,'chi.dat'))
        inv = np.linalg.inv(np.eye(3) - D @ zeta_true)
        chi_true = zeta_true @ inv
        if np.allclose(chi_true,chi_tequila):
            print('Chi (1e-8): \t\t\t\t=> OK')
        else:
            print('Chi (1e-8): \t\t\t\t=> ERROR')
            error += 1

    if error > 0:
        print('\n=> ERROR: Unit test failed {} sub-tests !\n'.format(error))
    else:
        print('\n=> SUCCESS: Unit test passed !\n')

    return error




errors = 0

calculations = ['charge','field','charge_field','charge_field_fulldiag','charge_field_anisotropic']
calculations += ['pbc_' + i for i in calculations]
calculations += ['zeta','zeta_fulldiag','zeta_epsilon']


try:
    np.printoptions(suppress=True)
except:
    pass


for name in calculations:

    try:

        string = "Testing: "+name+" (absolute tolerance)"
        print('\n'+string)
        print('-'*len(string))

        path = join(root,'test',name)

        ### Run Tequila ###
        print('Running tequila ...',end=' ')
        popen('cd {0}; {1} tequila.inp > tequila.out; cd {2}'.format(path, tequila, root))
        success = grep('SUCCESS',f'{path}/tequila.out')
        if success:
            print(' \t\t\t=> OK')
        else:
            print(' \t\t\t=> ERROR')
            print('Tequila execution failed !')

        # Test outputs
        errors += test(name)
    except Exception as err:
        print('Fatal Error while running test: ',name)
        print(err)
        exit(1)


if errors == 0:
    print('-'*54)
    print('\t\tSUCCESS: UNIT TESTS PASSED\t\t\t')
    print('-'*54)
else:
    print('-'*54)
    print('\t\tERROR: {} UNIT TESTS FAILED\t\t'.format(errors))
    print('-'*54)
