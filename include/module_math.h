int inverse(double* T, double* M, const int& N, const int& NRHS);

int eigen(double* T, double* W, const int& N);

void risposta(const double* P, const double* D, const double& V, const double& F, double* Z);

void solve(const double* T, const double* W, const double* S, double* M, const int& N);

void eigenvector_sum(const double* T, double* R, const int& N);

void spectral_zeta(const double* R, double* W, double* Z, const double& V, const double& epsilon, const int& N);

void inverse_zeta(const double* R, double* S, double* Z, const double& V, const double& epsilon, const int& N);

void matrix_inverse_3(const double* M, double* I);

void depolarization(const double* Z, const double* D, double* C);


double volume(const double* PBC_box);

inline void distance(const double* X, double* rij, double& r, const int& i, const int& j);

inline void distance(const double* X, const double* v, double* rij, double& r, const int& i, const int& j);



inline double dipole_dipole(const double* rij, const double& r, const double& epsilon, const int& k, const int& l);

inline double dipole_dipole_tinker(const double* rij, const double& r, const double& u, const double& epsilon, const int& k, const int& l);

inline double tinker_range_parameter(const double* P, const double& r, const int& i, const int& j,
                                     const int& m, const int& n);

inline void diagonal_block(double* T, const double* P, const int& m, const int& n, const int& i);

inline void depolarization(double* T, const double* D, const double& V, const int& n, const int& i, const int& j);


double electrostatic_energy(const double* Q, const double* V0, const int& n);

double induction_energy(const double* M, const double* S, const int& n);

void total_dipole(const double* M, double* D, const int& n);



void potential_field(const double* X, const double* Q, const double* F, double* V0, double* F0,
                     const double& epsilon, const int& n);

void potential_field(const double* X, const double* Q, const double* F, double* V0, double* F0,
                     const double& PBC_radius, const double* PBC_box, const double& epsilon, const int& n);

void potential_field(const double* X, const double* M, const double* V0, const double* F0, double* V, double* F,
                     const double& epsilon, const int& n);

void potential_field(const double* X, const double* M, const double* V0, const double* F0, double* V, double* F,
                     const double& PBC_radius, const double* PBC_box, const double& epsilon, const int& n);

void potential_field_zeta(const double* X, const double* M, const double& field, double* V, double* F,
                          const double& PBC_radius, const double* PBC_box, const double& epsilon, const int& n, const int& direction);



void interaction_matrix(const double* X, const double* P, double* T,
                        const double& epsilon, const int& m, const int& n, const double& hack);

void interaction_matrix(const double* X, const double* P, double* T,
                            const double& PBC_radius, const double* PBC_box,
                            const double& epsilon, const int& m, const int& n, const double& hack);


void source_vector(const double* X, const double* Q, const bool* B, double* S, double* V0, double* F0,
                   const double& epsilon, const double* F, const int& n);

void source_vector(const double* X, const double* Q, const bool* B, double* S, double* V0, double* F0,
                       const double& PBC_radius, const double* PBC_box,
                       const double& epsilon, const double* F, const int& n);
