#include <string>
using namespace std;

void print_header();

void print_license();

void center_text(const char* text);

void reading_text(const char* text);

string getFileName(const string path);

int read_input(const char* filename, string& data_file, double& PBC_radius, double* PBC_box,
  double& epsilon, double* F_ext, bool& writeT, bool& loadT, bool& loadS, bool& Fulldiag, bool& response,
  double* depol, double& hack);

int read_datainput(const string& filename, double*& X, double*& Q, bool*& B, double*& P, int& n, int& m);

int write_output(const string name, const double* X, const double* Q, const double* M, const int& n);

int save_vector(const char* filename, const double* x, const int& N);

int save_matrix(const char* filename, const double* x, const int& N);

int save_potential_field(const char* filename, const double* V, const double* F, const int& N);

int read_data(const char* filename, double* X);

void save_data(const char* filename, const double* x, const int& N);
